//TODO: move all of this logic to client
import request, {RequestPromise} from 'request-promise-native';

const apiBaseUrl = "https://api-v3.mbta.com";

function mbtaRequest(path: string, qs: any = {}): RequestPromise{
    return request.get({url:apiBaseUrl + path, qs: qs, json:true});
}

let stopsPromise: any = null;
function mbtaGetStops(forceRefresh: boolean = false): Promise<any> {
    if(!stopsPromise || forceRefresh)
    {
        stopsPromise = mbtaRequest("/stops", {});
    }
    return stopsPromise;
}

function arrayToObjectById(xs: any){
    return Array.from(xs).reduce((o: any, x: any) => {
        o[x.id] = x;
        return o;
    }, {});
}

let routesPromise: any = null;
export function mbtaGetRoutes(forceRefresh: boolean = false): Promise<any> {
    if(!routesPromise || forceRefresh)
    {
        routesPromise = mbtaRequest("/routes", {})
            .then(results => arrayToObjectById(results.data));
    }
    return routesPromise;
}

export function mbtaGetStations(): Promise<any> {
    return mbtaGetStops().then(stops => {
        let stations = stops.data.filter((stop: any) =>
            stop.attributes.location_type == 1);
        return stations;
    });
}
