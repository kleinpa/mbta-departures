import express from 'express';
import path from 'path';
import request, {RequestPromise} from 'request-promise-native';

import {mbtaGetRoutes, mbtaGetScheduleboard, mbtaGetStations} from './mbtaApi';
import fallback from 'express-history-api-fallback';

import webpack from 'webpack';
import webpackDevMiddleware from 'webpack-dev-middleware';
import webpackConfig from '../../client/webpack.config.js';

const app = express();
const webpackCompiler = webpack(webpackConfig);
app.use(webpackDevMiddleware(webpackCompiler, {}));

// TODO: use pass-through API and push this logic up to client
app.get('/data/stations', (req, res) =>
    mbtaGetStations().then((stations: any) => res.json(stations)));

app.use('/data', function(req, res) {
    const baseUrl = "https://api-v3.mbta.com";
    Object.assign(req.query, {"key": "4e282e705f5e4218b8a9b340e71e3a38"});
    request({url: baseUrl + req.path, qs: req.query}).pipe(res);
});

// Serves index.html for any other path, this seems like a common hack
app.use('*', (req, res, next) => {
    const filename = path.join(webpackCompiler.outputPath,'index.html');
    webpackCompiler.outputFileSystem.readFile(filename, function(err, result){
        if (err) {
            return next(err);
        }
        res.set('content-type','text/html');
        res.send(result);
        res.end();
    });
});

app.listen(3000, () => console.log('Server listening on port 3000!'));
