FROM node:10.6.0-alpine

RUN apk add --update \
  git \
 && rm -rf /var/cache/apk/*

ENV HOST=localhost:3000

WORKDIR /usr/src/app

COPY package.json package-lock.json ./
RUN npm install

COPY . .

CMD npm run start
