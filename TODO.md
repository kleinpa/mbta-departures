# Incomplete
- Add footer with info/credits
- Publish project source code
- Support colors other than red
- Fix styles.scss not loading
- Use off-the-shelf angular JSON API client
- Use Nginx for upstream API caching
- Reduce scope of rapidly-updated data
- Un-hardcode API key

# Future Work
- Add unit tests
- Production build process
- Reduce JS bundle size to something vaguely reasonable
- Tidy up station selection page and add route tags
- Don't rely on client clock for relative timing
- Display alerts for routes at station
- Plumb SSE data updates from the API through the server to the clients
- Enforce API cap
- Add filters for specific routes and platforms
- Use animations to make changes less jarring
- Decrease 

# Ideas
- Experiment with visualizations for incoming train data
- Add google analytics
- Figure out how to show items in two-column layout without bouncing around

 