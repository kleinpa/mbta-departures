import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HttpClientModule } from "@angular/common/http";
import { RouterModule, Routes } from "@angular/router";

import { StationSelectorComponent } from './station-selector/station-selector.component';
import { DeparturesComponent } from './departures/departures.component';
import { MbtaDataService } from "./data/mbta-data.service";
import { DepartureItemComponent } from './departure-item/departure-item.component';

const appRoutes: Routes = [
  { path: '', component: StationSelectorComponent },
  { path: 'station/:id/departures', component: DeparturesComponent},
];

@NgModule({
  declarations: [
    AppComponent,
    StationSelectorComponent,
    DeparturesComponent,
    DepartureItemComponent
  ],
  imports: [
    RouterModule.forRoot(appRoutes),
    BrowserModule,
    HttpClientModule,
  ],
  providers: [MbtaDataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
