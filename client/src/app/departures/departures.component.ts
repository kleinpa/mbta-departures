import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { map, first } from "rxjs/operators";
import { interval } from 'rxjs';

import { MbtaDataService } from "../data/mbta-data.service";

@Component({
  templateUrl: './departures.component.html',
  styleUrls: ['./departures.component.scss'],
})
export class DeparturesComponent implements OnInit {
  id: any;
  predictions: any = [];
  refreshInterval = 2000;
  refreshTimer: any;
  stop: any = null;

  constructor(private mbtaDataService: MbtaDataService, private activeRoute: ActivatedRoute) { }

  ngOnInit(): void {
    this.activeRoute.paramMap.subscribe(params => {
      // Get the stop id
      this.id = params.get("id");

      // Load the data, this will keep running while the page is loaded
      this.refresh();

      // Save the current stop object so it's name can be displayed
      this.mbtaDataService.getStation(this.id).subscribe(stop => this.stop = stop[0]);
    });
  }

  ngOnDestroy(): void {
    if(this.refreshTimer) {
      // not 100% sure if this is required
      this.refreshTimer.unsubscribe();
    }
  }

  getId = (x: any) => x.id;

  refresh(): void {
    // Load the updated predictions then start a timer for the next update
    this.mbtaDataService.getPredictions(this.id)
      .subscribe(predictions => {
        this.predictions = predictions.filter(x => x.attributes.departure_time);
        this.refreshTimer = interval(this.refreshInterval)
          .pipe(first())
          .subscribe(() => this.refresh());
      },
      err => {
        this.refreshTimer = interval(this.refreshInterval)
          .pipe(first())
          .subscribe(() => this.refresh())
      });
  }
}
