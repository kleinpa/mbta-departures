import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from "rxjs/operators";

// This class really should have used an off-the-shelf JSON API client.

@Injectable({
  providedIn: 'root'
})
export class MbtaDataService {

  constructor(private http: HttpClient) { }

  // simple http request to backend server
  apiRequest<T>(path: string, params: any = {}): Observable<T> {
    return this.http.get<T>('/data' + path, {params: params});
  }

  // permanently cache http request, probably not a great idea
  requestCache = new Map<string,Observable<any>>();
  cachedApiRequest<T>(path: string): Observable<T> {

    if(!this.requestCache.has(path)) { // cache miss
      this.requestCache.set(path, this.apiRequest(path));
    }

    return this.requestCache.get(path);
  }

  getRoutes(): Observable<any> {
    return this.cachedApiRequest('/routes');
  }

  getStations() {
    return this.cachedApiRequest('/stations')
      .pipe(map((x: any) => x));
  }

  getStation(id: any) {
    return this.getStations().pipe(
      map(x => {
        return Array.from(x).filter((x: any) => x.id == id);
      }));
  }

  // return a list of positions included all data needed for the live display
  // this logic should live elsewhere
  getPredictions(stopId: any) {
    return this.http.get("/data/predictions", {params: {
        "filter[stop]": stopId,
        "include": "route,stop,trip,vehicle",
        "sort": "departure_time"}}).pipe(
      map((x: any) => this.inflate(x))
    );
  }

  inflate(result: any): any {
    const resultData: Array<any> = result.data;
    const includeData: Array<any> = result.included;

    // sort included by type into maps by id
    const includedMap = new Map();
    const includedMapAdd = (x: any) => {
      if(!includedMap.has(x.type)) {
        includedMap.set(x.type, new Map());
      }
      includedMap.get(x.type).set(x.id, x);
    }

    includeData.forEach(includedMapAdd);
    resultData.forEach(includedMapAdd);

    // replace the relationships items with the actual objects where possible
    const replaceRelationships = (x: any) => {
      Object.keys(x.relationships || {}).forEach(k => {
        const ref = x.relationships[k];
        if(ref.data && includedMap.has(ref.data.type)) {
          if(includedMap.has(ref.data.type)){}
          x.relationships[k] = includedMap.get(ref.data.type).get(ref.data.id);
        }
      });
    }

    includeData.forEach(replaceRelationships);
    resultData.forEach(replaceRelationships);

    return resultData;
  }



}
