import { TestBed, inject } from '@angular/core/testing';

import { MbtaDataService } from './mbta-data.service';

describe('MbtaDataService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MbtaDataService]
    });
  });

  it('should be created', inject([MbtaDataService], (service: MbtaDataService) => {
    expect(service).toBeTruthy();
  }));
});
