import { Component, Input, OnInit } from '@angular/core';
import { interval } from 'rxjs';
import * as moment from 'moment';

@Component({
  selector: 'app-departure-item',
  template: `
    <div class="departure-item" [ngClass]="{
    'background-red-line':prediction.relationships.route.id === 'Red',
    'background-blue-line':prediction.relationships.route.id === 'Blue',
    'background-orange-line':prediction.relationships.route.id === 'Orange',
    'background-green-line':prediction.relationships.route.attributes.type === 0,
    'background-commuter-rail':prediction.relationships.route.attributes.type === 2,
    'background-bus':prediction.relationships.route.attributes.type === 3,
    'background-ferry':prediction.relationships.route.attributes.type === 4
    }">
      <div class="route">{{routeName}} <span class="route-to">to</span></div>
      <div class="headsign">{{prediction.relationships.trip.attributes.headsign}} </div>
      <div class="status">
        <div class="statusTime">{{statusTimeText}}</div>
        <div class="statusText">{{statusTypeText}}</div>
      </div>
    </div>
  `,
  styleUrls: ['./departure-item.component.scss'],
})
export class DepartureItemComponent implements OnInit {
  @Input() prediction: any;

  statusTypeText: string = "";
  statusTimeText: string = "";
  routeName: string = "";

  updateTimer: any;

  constructor() { }

  ngOnInit() {
    this.updateTimer = interval(1000)
      .subscribe(() => this.buildStatus());
  }

  formatTimeDiff(dt): string {
    const ms = moment(dt).diff(moment.now())
    if ( ms < (1000)) {
      return "now";
    } else if (ms > (40 * 60 * 1000)) { // show time over 40 minutes, I made that up
      return moment(dt).format("h:mm a");
    } else if (ms > (60 *1000)) {
      return Math.ceil(ms / (60*1000)) + " min";
    } else {
      return Math.ceil(ms / (1000)) + " sec";
    }
  }

  isArriving(prediction) {
    if(prediction.relationships.stop
      && prediction.relationships.vehicle
      && prediction.relationships.vehicle.relationships == prediction.relationships.stop){
      return prediction.relationships.vehicle.attributes.current_status
    }
  }

  buildStatus() {
    this.routeName = this.prediction.relationships.route.attributes.short_name
      || this.prediction.relationships.route.attributes.long_name;


    if(!this.prediction) {
      this.statusTimeText = "";
      this.statusTypeText = "";
    } else {
      const toArrival = this.prediction.attributes.arrival_time
        && moment(this.prediction.attributes.arrival_time).diff(moment.now())

      const toDeparture = this.prediction.attributes.departure_time
        && moment(this.prediction.attributes.departure_time).diff(moment.now())

      if(toArrival && toArrival > (1000)){
        this.statusTypeText= "arrives";
        this.statusTimeText = this.formatTimeDiff(this.prediction.attributes.arrival_time);
        return;
      } else if (toDeparture > (1000)) {
        if(this.prediction.attributes.status == "Now boarding" ||
          this.prediction.attributes.status == "All aboard" ||
          toArrival < 0) {
          this.statusTypeText= "boarding";
        } else {
          this.statusTypeText= "departs";
        }
        this.statusTimeText = this.formatTimeDiff(this.prediction.attributes.departure_time);
      } else {
        this.statusTypeText= "departing";
        this.statusTimeText = this.formatTimeDiff(this.prediction.attributes.departure_time);
      }
    }
  }
}
