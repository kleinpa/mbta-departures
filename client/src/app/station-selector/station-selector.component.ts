import { Component, OnInit } from '@angular/core';

import { MbtaDataService } from "../data/mbta-data.service";

@Component({
  selector: 'app-station-selector',
  templateUrl: './station-selector.component.html',
  styleUrls: ['./station-selector.component.scss'],
})
export class StationSelectorComponent implements OnInit {

  stations: any;

  constructor(private mbtaDataService: MbtaDataService) { }

  ngOnInit(): void {
    this.mbtaDataService.getStations().subscribe(stations => this.stations = stations);
  }
}
