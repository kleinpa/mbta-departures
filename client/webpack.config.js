const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { AngularCompilerPlugin } = require('@ngtools/webpack');

module.exports = {
  context: path.resolve(__dirname),
  resolve: { extensions: [".ts", ".js"]},
  mode: "development",
  entry: {
    polyfills: ["./src/polyfills.ts"],
    main: ["./src/main.ts"],
    styles: ["./src/styles.scss"],
  },
  output: {
    path: path.resolve(__dirname, "dist"),
    filename: "[name].bundle.js",
    chunkFilename: "[id].chunk.js"
  },
  module: {
    rules: [
      {
        test: /\.html$/,
        loader: "raw-loader"
      },
      {
        exclude: ["src/styles.scss"],
        test: /\.scss$|\.sass$/,
        //use: ["style-loader", "css-loader", "sass-loader"],
        use: ['raw-loader', 'sass-loader'],
      },
      {
        include: ["src/styles.scss"],
        test: /\.scss$|\.sass$/,
        //use: ["style-loader", "css-loader", "sass-loader"],
        use: ['raw-loader', 'sass-loader'],
      },
      {
        test: /(?:\.ngfactory\.js|\.ngstyle\.js|\.ts)$/,
        loader: "@ngtools/webpack",
        exclude: [/\.(spec)\.ts$/],
      }
    ]
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: "./src/index.html",
    }),
    new AngularCompilerPlugin({
      tsConfigPath: path.resolve(__dirname, './src/tsconfig.app.json'),
      entryModule: 'src/app/app.module#AppModule',
      sourceMap: true,
      skipCodeGeneration: true,
    })
  ],
};
