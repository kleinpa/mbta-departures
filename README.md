MBTA Departure Board
====================

This is a web-based departure board for MBTA stations using data from
the [MBTA v3 API](https://api-v3.mbta.com/). It shows a live view of
all vehicles leaving a particular MBTA station

This project is built with:

- Angular 6
- Node.js
- Express
- Docker
- Nginx

Read [TODO.md](./TODO.md) for some of the things I haven't yet had a
chance to implement.


### Getting Started

If `docker` and `docker-compose` are installed getting off the ground
should be easy:

```bash
$ docker-compose up
```
